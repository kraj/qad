# OpenQA Q.A.D. backend

To install, simple copy the contents of this folder into your `os-autoinst` directory (e.g `/usr/lib64/os-autoinst/`)

To use, target a "qad" backend type with the following settings:

 - `QAD_SCREEN_NO=$ilm_screen_number` the ilm screen to use
 - `QAD_TOUCH_DEVICE_NO=$touch_number` the evdev device ID to send input via
 - `QAD_KEYBOARD_DEVICE_NO=$keyboard_number` evdev keyboard device id
 - `QAD_SERVER_ADDRESS=$RIG_IP` the full url of the running QAD daemon (e.g http://localhost:1234)
