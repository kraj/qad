#!/usr/bin/perl
#
# qad.pm
#
# Backend for a simple HTTP-driven generic daemon we can deploy to arbitrary
# hardware.  (Ab)Uses /dev/input/event* to control the SUT, and takes
# screenshots via Weston's ILM API.
#
# Currently only mouse (touchscreen) and basic keyboard events are
# implemented.

package backend::qad;

use Mojo::Base 'backend::baseclass', -signatures;
use bmwqemu qw(diag fctwarn);

sub new ($class)
{
    my $self = $class->SUPER::new;

    diag 'qad.pm BE starting';
    return $self;
}

sub do_start_vm ($self, @) {
    my $qad = $testapi::distri->add_console(
        'sut',
        'qad',
        {
            screeneventno => int($bmwqemu::vars{QAD_SCREEN_NO} // 1),
            touchdeviceeventno => int($bmwqemu::vars{QAD_TOUCH_DEVICE_NO} // 1),
            keyboarddeviceeventno => int($bmwqemu::vars{QAD_KEYBOARD_DEVICE_NO} // 1),
            serverurl => $bmwqemu::vars{QAD_SERVER_ADDRESS} // "http://localhost:8888",
            x_scale => $bmwqemu::vars{QAD_X_SCALE} // 1,
            y_scale => $bmwqemu::vars{QAD_Y_SCALE} // 1
        });
    $qad->backend($self);
    $self->select_console({testapi_console => 'sut'});
    $self->truncate_serial_file;
    return {};
}

sub do_stop_vm ($self, @) {
    return {};
}

1;
